<%@page import="java.sql.Connection"%>
<%@page import="com.model.DBConn"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>CSV Reader</title>
        <link href="assets/css/Main.css" rel="stylesheet" type="text/css"/>
        <script src='//ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
        <script src="../../assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="assets/js/Main.js" type="text/javascript"></script>
    </head>
    <body>
        <div class="full-BG">
            <div class="container page-wrap">
                <h1 class="text-center" style="color: #0078ff;"><b>CSV READER  <i class="fa fa-file-excel-o" ></i></b></h1>
                <br/>
                <h3 class="text-center">This CSV Reader will parse the data from the file and insert it to a database. It will also display the statistics of the import.  </h3>
                <br/>
                <br/>
                <br/>
                <div class="form-group">
                    <div class="row">
                        <div id="total" class="col-sm-3 col-sm-offset-1 text-center">
                            <h3>${totalRecords}</h3>
                            <br/>
                            <label>Number of Records Received</label>
                        </div>
                        <div id="success" class="col-sm-3 text-center">
                            <h3>${numOfSuccessfulRecords}</h3>
                            <br/>
                            <label>Number of Successful Records</label>
                        </div>
                        <div id="failed" class="col-sm-3 text-center">
                            <h3>${numOfFailedRecords}</h3>
                            <br/>
                            <label>Number of Failed Records</label>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
                <br/>
                <div class="alert alert-info text-center"><h4><i class="fa fa-info-circle"></i> ${message}</h4></div>  
                <form method="POST" action="${pageContext.request.contextPath}/index.htm" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>Select file: </label>
                                <div class="input-group">                           
                                    <input id="txtCSVFile"  name="txtCSVFile" class="form-control" type="text" readonly/>
                                    <label class="input-group-btn">
                                        <span class="btn btn-file"><i class="fa fa-upload"></i>
                                            <input type="file" style="display: none;" name="file"/></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-sm-6">                       
                                <input id="importCSV" type="submit" class="btn btn-teal" value="IMPORT"/>
                            </div>
                        </div>
                    </div>
                </form>
                <br/>
            </div>
        </div>
    </body>
</html>
