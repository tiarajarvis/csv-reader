package com.model;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.List;

public class DBConn {

    public Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/ms3", "root", "root");
            return conn;
        } catch (ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConn.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public void insertCSV(DBConn conn, List<CSVFile> file) throws SQLException {
        Connection c = getConnection();
        String query = "INSERT INTO csvdata (A, B, C, D, E, F, G, H, I, J)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            for (CSVFile f : file) {
                PreparedStatement stmt;
                stmt = c.prepareStatement(query);
                stmt.setString(1, f.getA());
                stmt.setString(2, f.getB());
                stmt.setString(3, f.getC());
                stmt.setString(4, f.getD());
                stmt.setBlob(5, f.getE());
                stmt.setString(6, f.getF());
                stmt.setBigDecimal(7, f.getG());
                stmt.setBoolean(8, f.getH());
                stmt.setBoolean(9, f.getI());
                stmt.setString(10, f.getJ());
                stmt.execute();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DBConn.class.getName()).log(Level.SEVERE, null, ex);
            //System.out.print("DB: " + ex);
        } finally {
            if (c != null) {
                c.close();
            }
        }
    }
}
