package com.model;

import java.math.BigDecimal;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.List;

public class CSVFile {

    private String a;
    private String b;
    private String c;
    private String d;
    private Blob e;
    private String f;
    private  BigDecimal g;
    private Boolean h;
    private Boolean i;
    private String j;
    private static final char DEFAULT_SEPARATOR = ',';
    private static final char DEFAULT_QUOTE = '"';

    public CSVFile(){
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = false;
        this.i = false;
        this.j = null;      
    }
    
    public CSVFile(String a, String b, String c, String d, Blob e, String f, BigDecimal g, Boolean h, Boolean i, String j){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }
    
    public String getA() {
        return a;
    }
    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }
    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }
    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }
    public void setD(String d) {
        this.d = d;
    }

    public Blob getE() {
        return e;
    }
    public void setE(Blob e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }
   public void setF(String f) {
        this.f = f;
    }
   
   public BigDecimal getG() {
        return g;
    }
   public void setG(BigDecimal g) {
        this.g = g;
    }
   
    public Boolean getH() {
        return h;
    }
    public void setH(Boolean h) {
        this.h = h;
    }

    public Boolean getI() {
        return i;
    }
    public void setI(Boolean i) {
        this.i = i;
    }
    
     public String getJ() {
        return j;
    }
    public void setJ(String j) {
        this.j = j;
    }
    
    //PARSE METHODS
    public static List<String> parseLine(String cvsLine) {
        return parseLine(cvsLine, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
    }

    public static List<String> parseLine(String cvsLine, char separators) {
        return parseLine(cvsLine, separators, DEFAULT_QUOTE);
    }

    public static List<String> parseLine(String cvsLine, char separators, char customQuote) {

        List<String> result = new ArrayList<>();

        //if empty, return!
        if (cvsLine == null && cvsLine.isEmpty()) {
            return result;
        }

        if (customQuote == ' ') {
            customQuote = DEFAULT_QUOTE;
        }

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuffer curVal = new StringBuffer();
        boolean inQuotes = false;
        boolean startCollectChar = false;
        boolean doubleQuotesInColumn = false;

        char[] chars = cvsLine.toCharArray();

        for (char ch : chars) {

            if (inQuotes) {
                startCollectChar = true;
                if (ch == customQuote) {
                    inQuotes = false;
                    doubleQuotesInColumn = false;
                    curVal.append('"');
                } else //Fixed : allow "" in custom quote enclosed
                 if (ch == '\"') {
                        if (!doubleQuotesInColumn) {
                            curVal.append(ch);
                            doubleQuotesInColumn = true;
                        }
                    } else {
                        curVal.append(ch);
                    }
            } else if (ch == customQuote) {

                inQuotes = true;

                //Fixed : allow "" in empty quote enclosed
                if (chars[0] != '"' && customQuote == '\"') {
                    curVal.append('"');
                }

                //double quotes in column will hit this!
                if (startCollectChar) {
                    curVal.append('"');
                }

            } else if (ch == separators) {

                result.add(curVal.toString());

                curVal = new StringBuffer();
                startCollectChar = false;

            } else if (ch == '\r') {
                //ignore LF characters
                continue;
            } else if (ch == '\n') {
                //the end, break!
                break;
            } else {
                curVal.append(ch);
            }
        }
        result.add(curVal.toString());
        return result;
    }
}
