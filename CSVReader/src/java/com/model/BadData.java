package com.model;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BadData {
    private String a;
    private String b;
    private String c;
    private String d;
    private String e;
    private String f;
    private  String g;
    private String h;
    private String i;
    private String j;
    private static final char DEFAULT_SEPARATOR = ',';
    private static final char DEFAULT_QUOTE = '"';
    
    public BadData(){
        this.a = null;
        this.b = null;
        this.c = null;
        this.d = null;
        this.e = null;
        this.f = null;
        this.g = null;
        this.h = null;
        this.i = null;
        this.j = null;      
    }
    
    public BadData(String a, String b, String c, String d, String e, String f, String g, String h, String i, String j){
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
        this.g = g;
        this.h = h;
        this.i = i;
        this.j = j;
    }
    
    public String getA() {
        return a;
    }
    public void setA(String a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }
    public void setB(String b) {
        this.b = b;
    }

    public String getC() {
        return c;
    }
    public void setC(String c) {
        this.c = c;
    }

    public String getD() {
        return d;
    }
    public void setD(String d) {
        this.d = d;
    }

    public String getE() {
        return e;
    }
    public void setE(String e) {
        this.e = e;
    }

    public String getF() {
        return f;
    }
   public void setF(String f) {
        this.f = f;
    }
   
   public String getG() {
        return g;
    }
   public void setG(String g) {
        this.g = g;
    }
   
    public String getH() {
        return h;
    }
    public void setH(String h) {
        this.h = h;
    }

    public String getI() {
        return i;
    }
    public void setI(String i) {
        this.i = i;
    }
    
     public String getJ() {
        return j;
    }
    public void setJ(String j) {
        this.j = j;
    }
    
    public void insertBadData(List<BadData> badData, String filePath) throws IOException {
        FileWriter writer = new FileWriter(filePath);
        for (BadData bd : badData) {
            List<String> list = new ArrayList<>();
            list.add(bd.getA());
            list.add(bd.getB());
            list.add(bd.getC());
            list.add(bd.getD());
            list.add(bd.getE());
            list.add(bd.getF());
            list.add(bd.getG());
            list.add(bd.getH());
            list.add(bd.getI());
            list.add(bd.getJ());
            CSVUtils.writeLine(writer, list, DEFAULT_SEPARATOR, DEFAULT_QUOTE);
        }
        writer.flush();
        writer.close();
    }
}
