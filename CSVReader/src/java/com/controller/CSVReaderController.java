package com.controller;

import com.model.BadData;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import com.model.CSVFile;
import com.model.DBConn;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Blob;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/index")
public class CSVReaderController {

    DBConn db = new DBConn();
    List<CSVFile> successfulList = new ArrayList();
    List<BadData> failedList = new ArrayList();
    NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
    private static String UPLOADED_FOLDER = "C://temp//";
    String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView index() {
        CSVFile file = new CSVFile();
        ModelAndView model = new ModelAndView("index");
        model.addObject("totalRecords", 0);
        model.addObject("numOfSuccessfulRecords", 0);
        model.addObject("numOfFailedRecords", 0);
        model.addObject("message", "No messages at this time.");
        return model;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ModelAndView processImport(@RequestParam("file") MultipartFile file, ModelAndView model) {
        if (file.isEmpty()) {
            model.addObject("message", "Please select a file to upload.");
            model.addObject("totalRecords", 0);
            model.addObject("numOfSuccessfulRecords", 0);
            model.addObject("numOfFailedRecords", 0);
            return model;
        } else if (!file.getContentType().equalsIgnoreCase("application/vnd.ms-excel")) {
            model.addObject("message", "Please select a <b>.csv</b> file.");
            model.addObject("totalRecords", 0);
            model.addObject("numOfSuccessfulRecords", 0);
            model.addObject("numOfFailedRecords", 0);
        } else {
            try {
                //UPLOAD FILE
                File theDir = new File(UPLOADED_FOLDER);
                if (!theDir.exists()) {
                    theDir.mkdir();
                }
                byte[] bytes = file.getBytes();
                Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
                Files.write(path, bytes);

                //PARSE FILE
                String csvFilePath = UPLOADED_FOLDER + file.getOriginalFilename();
                int total = 0;
                int success = 0;
                int failed = 0;
                BufferedReader br = new BufferedReader(new FileReader(csvFilePath));
                String csvLine = null;
                Blob image = db.getConnection().createBlob();
                while ((csvLine = br.readLine()) != null) {
                    if (csvLine.isEmpty() || csvLine.trim().equals("")) {
                        break;
                    } else {
                        total++;
                            CSVFile csvFile = new CSVFile();
                            List<String> line = csvFile.parseLine(csvLine);

                            //CREATE SUCCESS & FAILED OBJECT LISTS  
                            if (!line.contains("") && !line.get(0).equals("A")) {
                                BigDecimal price = new BigDecimal(nf.parse(line.get(6)).toString());
                                byte[] byteImg = line.get(4).getBytes("UTF-8");                               
                                image.setBytes(1, byteImg);
                                csvFile = new CSVFile(line.get(0), line.get(1), line.get(2), line.get(3), image, line.get(5), price, Boolean.parseBoolean(line.get(7)), Boolean.parseBoolean(line.get(8)), line.get(9));
                                successfulList.add(csvFile);
                                db.getConnection().close();
                                success++;
                            } else {
                                BadData badData;
                                badData = new BadData(line.get(0), line.get(1), line.get(2), line.get(3), line.get(4), line.get(5), line.get(6), line.get(7), line.get(8), line.get(9));
                                failedList.add(badData);
                                failed++;
                            }
                    }
                }
                db.insertCSV(db, successfulList);
                BadData badData = new BadData();
                badData.insertBadData(failedList, UPLOADED_FOLDER + "bad-data " + timeStamp + ".csv");
                model.addObject("totalRecords", total);
                model.addObject("numOfSuccessfulRecords", success);
                model.addObject("numOfFailedRecords", failed);
                model.addObject("message", "Your file has been successfully imported.");
            } catch (Exception e) {
                //System.out.print(e);
                model.addObject("message", e);
            }
        }
        return model;
    }
}
