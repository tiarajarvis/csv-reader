# README #

### What is this repository for? ###

CSV Reader Web Application in Java using Spring framework MVC and MySQL Database


### Default Message ###
![Scheme](images/default.png)


### No File Message ###
![Scheme](images/no-file.png)


### Wrong File Type Message ###
![Scheme](images/wrong-file-type.png)


### Successful Import Message ###
![Scheme](images/imported.png)